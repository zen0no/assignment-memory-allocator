#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

_Noreturn void err( const char* msg, ... ) {
  va_list args;
  va_start (args, msg);
  vfprintf(stderr, msg, args);
  va_end (args);
  abort();
}


extern inline size_t size_max( size_t x, size_t y );




void debug_fputs(FILE* f, const char* msg){
    #ifdef DEBUG
    fputs(msg, f);

    #else
    (void) msg; (void) f;
    #endif

}

void debug_fprintf(FILE* f, const char* msg, ...){
    #ifdef DEBUG

    va_list args;
    va_start(args, msg);
    vfprintf(f, msg, args);
    va_end(args);

    #else
    (void) msg; (void) f;
    #endif

}