BUILD_DIR=build
SRC_DIR=src
INCLUDE_DIR = include

CFLAGS=--std=c17 -I include $(INCLUDE_DIRs) -Wall -pedantic -Isrc/ -ggdb -Wextra -Werror

debug: CFLAGS += -DDEBUG

SRCS  =  $(wildcard $(SRC_DIR)/*.c $(SRC_DIR)/**/*.c)

BUILD_TARGET = main

CC=gcc

all: build build/app

debug: build build/app	

build:
	mkdir -p $(BUILD_DIR)

build/app: $(SRCS)
		$(CC) $(CFLAGS) -o $(BUILD_DIR)/$(BUILD_TARGET) $^

run:
	./$(BUILD_DIR)/$(BUILD_TARGET)

clean:
	rm -rf $(BUILD_DIR)

