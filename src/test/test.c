
#define _DEFAULT_SOURCE


#include <inttypes.h>
#include <stdio.h>
#include <sys/mman.h>


#include "mem.h"
#include "mem_internals.h"
#include "memory.h"
#include "test/test.h"
#include "util.h"

/*
    Utilitiy functions for tests
*/

static struct block_header *get_block_header(const void *block) {
    return (struct block_header *) (((uint8_t *) block) - offsetof(struct block_header, contents));
}

static bool block_is_free(const void *block) {
    struct block_header *header = get_block_header(block);

    return header->is_free;
}


static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static void *map_region(void *addr) {
    return mmap((void *) addr, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANON, 0, 0);
}

static struct block_header *find_border_block(struct block_header *header){
    struct block_header *block_border = header;
    while (block_border->next){
        block_border = block_border->next;
    }
    return block_border;
    
}



/*

    TESTS

*/


/*
    One block allocated
*/

static bool test_1(FILE* f) {
    void *block_1 = _malloc(300);
    bool block_1_allocated = !block_is_free(block_1);    

    debug_fputs(f, "One block allocated\n");
    debug_heap(f, HEAP_START);


    _free(block_1);
    debug_fputs(f, "One block freed\n");
    debug_heap(f, HEAP_START);

    bool block_1_freed = block_is_free(block_1);

    return block_1_allocated && block_1_freed;
}


/*
    Two blocks allocated
*/

static bool test_2(FILE* f) {
    void *block_1 = _malloc(300);
    void *block_2 = _malloc(300);
    bool blocks_allocated = !block_is_free(block_1) && !block_is_free(block_2);
    debug_fputs(f, "Two blocks allocated\n");
    debug_heap(f, HEAP_START);

    _free(block_2);
    bool block_2_freed = block_is_free(block_2);
    bool block_1_still_allocated = !block_is_free(block_1);
    debug_fputs(f, "One block allocated, one freed\n");
    debug_heap(f, HEAP_START);

    
    _free(block_1);

    return blocks_allocated && block_2_freed && block_1_still_allocated;
}


/*
    Three blocks allocated and two non-continious block freed"
*/

static bool test_3(FILE* f) {
    void *block_1 = _malloc(300);
    void *block_2 = _malloc(300);
    void *block_3 = _malloc(300);
    bool blocks_init_allocated = !block_is_free(block_1) && !block_is_free(block_2) && !block_is_free(block_2);
    debug_fputs(f, "Three blocks allocated \n");
    debug_heap(f, HEAP_START);


    _free(block_3);
    _free(block_1);
    bool block_3_free = block_is_free(block_3);
    bool block_1_free = block_is_free(block_1);
    bool block_2_still_allocated = !block_is_free(block_2);
    debug_fputs(f, "Two non-continious block freed\n");
    debug_heap(f, HEAP_START);

    _free(block_2);




    return blocks_init_allocated && block_1_free && block_3_free && block_2_still_allocated;

}

/*
    Three blocks allocated and two continious blocks freed.
*/

static bool test_4(FILE* f) {

    void *block_1 = _malloc(300);

    void *block_2 = _malloc(300);

    void *block_3 = _malloc(300);
    bool blocks_init_allocated = !block_is_free(block_1) && !block_is_free(block_2) && !block_is_free(block_2);
    debug_fputs(f, "Three blocks allocated \n");
    debug_heap(f, HEAP_START);



    _free(block_2);
    _free(block_1);
    bool block_1_free = block_is_free(block_1);
    bool block_2_free = block_is_free(block_2);
    bool block_3_still_allocated = !block_is_free(block_3);

    struct block_header *bheader = get_block_header(block_1);
    size_t capacity = bheader->capacity.bytes;

    bool block_are_merged = (capacity == 600 + offsetof(struct block_header, contents));



    debug_fputs(f, "Two continious block freed\n");
    debug_heap(f, HEAP_START);


    _free(block_3);

    return blocks_init_allocated
     && block_1_free && block_2_free && block_3_still_allocated
      & block_are_merged;

}

/*
    Two blocks alloacted. One block is larger thar REGION_MIN_CAPACITY. New region mapped after previous
*/

 static bool test_5(FILE* f) {


    void *block_1 = _malloc(300);
    struct block_header *block_1_header = get_block_header(block_1);
    struct block_header *border_header = find_border_block(block_1_header);
    
    void *block_2 = _malloc(REGION_MIN_SIZE + 1);
    struct block_header *block_2_header = get_block_header(block_2);
    
    debug_fputs(f, "Large and normal blocks allocated\n");
    debug_heap(f, HEAP_START);


    size_t block_2_capacity = block_2_header->capacity.bytes;
    debug_heap(f, HEAP_START);

    _free(block_1);
    _free(block_2);


    return block_2_capacity == REGION_MIN_SIZE + 1 &&
           block_2_header == block_after(border_header);
}


/*
    Two blocks alloacted. One block is larger thar REGION_MIN_CAPACITY. New region don't continue previous
*/


static bool test_6(FILE* f) {


    void *block_1 = _malloc(300);
    struct block_header *bheader_1 = get_block_header(block_1);
    debug_fputs(f, "One block allocated\n");

    void *addr_after_block_1 = block_after(bheader_1);

    struct block_header *last_heap_header = bheader_1;
    while (last_heap_header->next) {
        last_heap_header = last_heap_header->next;
    }



    uint8_t *head_border_addr = last_heap_header->capacity.bytes + last_heap_header->contents;

    map_region(head_border_addr);
    debug_fputs(f, "Dummy region mapped\n");

    void *block_2 = _malloc(REGION_MIN_SIZE * 2);
    struct block_header *block_2_header = get_block_header(block_2);
    debug_fputs(f, "One large block allocated. New region mapped\n");
    debug_heap(f, HEAP_START);

    _free(block_1);
    _free(block_2);
    return addr_after_block_1 != (void *) block_2_header;
}



enum test_status test_all(FILE *f){
    
    bool tests_success = true;

    bool (*tests[])(FILE *f) = {
        &test_1,
        &test_2,
        &test_3,
        &test_4,
        &test_5,
        &test_6
    };

    for(uint16_t j = 0; j <6; j++){

        fprintf(f, "TEST %"PRIu16"\n\n", j + 1);

        if (tests[j](f)){
            fputs("PASSED\n\n", f);        }

        else {
            tests_success = false;
            fputs("FAILED\n\n", f);
        }
    }

    return (tests_success) ? TEST_SUCCESSED : TEST_FAILED;

    }
