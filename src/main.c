#include <stdio.h>
#include "test/test.h"
#include "mem.h"
#include "mem_internals.h"

int main(void) {
    heap_init(REGION_MIN_SIZE);

    enum test_status status = test_all(stderr);

    if (status == TEST_FAILED){
        return -1;
    }

    return 0;

}
